import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'

import { HomeComponent } from './Pages/home/home.component'
import { CrearComponent } from './Pages/crear/crear.component'
import { ListComponent } from './Pages/list/list.component'
import { EditComponent } from './Pages/edit/edit.component'
import { ListResponseComponent } from './Response/list/list.component'

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'crear',
    component: CrearComponent
  },
  {
    path: 'listar',
    component: ListComponent
  },
  {
    path: 'editar',
    component: EditComponent
  },
  {
    path: 'lista_mantenimiento',
    component: ListResponseComponent
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
