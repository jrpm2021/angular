import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import swal from 'sweetalert2'
import request from 'superagent'

@Component({
  selector: 'app-crear',
  templateUrl: './crear.component.html',
  styleUrls: ['./crear.component.scss']
})
export class CrearComponent implements OnInit {

  apellido:string='';
  nombre:string='';
  cargo:string='';
  genero:string=''
  edad:number=0;

  constructor(private root: Router) { }

  ngOnInit(): void {
    
  }

  onClickCrear(){

    if(this.nombre && this.cargo && this.apellido && this.edad){
      request.post('http://10.10.1.49/repotest/code/control.php?case=guardar')
      .set("content-type", "application/x-www-form-urlencoded")
      .send({
        nombre: this.nombre,
        cargo: this.cargo,
        genero: this.genero,
        apellido: this.apellido,
        edad: this.edad
      }).then(response =>{
        console.log(response)
        swal.fire({
          icon: 'success',
          title: 'Ok',
          text: 'Usuario Creado',
        }).then(() =>{
          this.root.navigateByUrl("listar")
        })
      }).catch(err =>{
        swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'No se pudo crear el usuario',
        })
      })         
    }else{
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Campos Faltantes',
      })  
    }
     
  }

}
