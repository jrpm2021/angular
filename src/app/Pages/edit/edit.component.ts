import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import request from 'superagent'
import swal from 'sweetalert2'

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  usuarios = {};
  id:number = 0;  
  nombre:string = '';
  cargo:string ='';
  edad:number =0;
  apellido:string ='';

  constructor(private root: Router) { 

  }

  ngOnInit(): void {
    request.get('http://10.10.1.49/repotest/code/control.php?case=load')
    .then(response =>{      
      this.usuarios = response.body.filter(index => index.id == localStorage.getItem('id'))
      console.log(this.usuarios[0])
      this.id=this.usuarios[0].id
      this.nombre = this.usuarios[0].nombre
      this.edad = this.usuarios[0].edad
      this.cargo = this.usuarios[0].cargo
      this.apellido = this.usuarios[0].apellido
    }).catch(err =>{
      console.log(err)
    })
  }

  onClickActualizar(){
    if(this.id && this.nombre && this.cargo){
      request.post('http://10.10.1.49/repotest/code/control.php?case=actualizar')
      .set("content-type", "application/x-www-form-urlencoded")
      .send({
        nombre: this.nombre,
        id: this.id,
        cargo: this.cargo,
        edad: this.edad,
        apellido: this.apellido
      }).then(response =>{
        console.log(response)
        swal.fire({
          icon: 'success',
          title: 'Ok',
          text: 'Usuario Editado',
        }).then(() =>{
          this.root.navigateByUrl("listar")
        })
      }).catch(err =>{
        console.log(err)
      })         
    }else{
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!',
      })  
    }
  }

}
