
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import request from 'superagent'
import swal from 'sweetalert2'

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit {
  
  usuarios:[]=[];
  
  
  constructor(private root: Router) {
   }

   editUser( user ){
     localStorage.setItem('id', user.id)
     this.root.navigateByUrl('editar') 
   }
   
   deleteUser(user){
    request.post('http://10.10.1.49/repotest/code/control.php?case=delete')
    .set("content-type", "application/x-www-form-urlencoded")
    .send({id: user.id})
    .then(response =>{
      this.root.navigateByUrl("listar")
      this.ngOnInit()
    })
    .catch(err =>{
      console.log(err)
    })
   }

  ngOnInit(): void {
    request.get('http://10.10.1.49/repotest/code/control.php?case=load')
    .then(response =>{      
      console.log(response)
      this.usuarios=response.body
    }).catch(err =>{
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Usuarios no disponibles',
      }).then( response => {
        this.root.navigateByUrl("")
      })      
    })
  }

  

}

